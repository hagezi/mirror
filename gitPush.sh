#!/bin/bash

comment=$1
if [ -z "$1" ]; then
    comment="update"
fi

localgit=/media/nas/git/mirror
cd $localgit || exit
git pull
git add .
git commit -m "$comment"
git push origin main

echo ""
